#include "util.h"

unsigned char *LoadFileToFast(char *filename)
{
	FILE *f = fopen(filename, "rb");
	fseek(f, 0, SEEK_END);
	long fsize = ftell(f);
	fseek(f, 0, SEEK_SET);
	
	printf("File %s : %ld bytes\n", filename, fsize);
	unsigned char * mem = AllocMem(fsize+1, MEMF_FAST | MEMF_CLEAR);
	fread(mem, 1, fsize, f);
	fclose(f);
	
	return mem;
}

struct BitmapData *CreateBitmapData(int width, int height, UBYTE planes)
{
	struct BitmapData *bitmapData = AllocMem(sizeof(struct BitmapData), MEMF_CHIP | MEMF_CLEAR);
	bitmapData->width = width;
	bitmapData->height = height;
	bitmapData->planes = planes;	
	bitmapData->bitmap = AllocBitMap(width, height, planes, BMF_CLEAR | BMF_DISPLAYABLE, NULL);
	bitmapData->cm = GetColorMap(16);
	LONG colorTableSize=16;
	
	bitmapData->rasInfo.BitMap = bitmapData->bitmap;
	bitmapData->rasInfo.RxOffset = 0;
	bitmapData->rasInfo.RyOffset = 0;
	
	return bitmapData;
}

struct BitmapData *LoadBitmapData(char *filename)
{	
	struct BitmapData *bitmapData = AllocMem(sizeof(struct BitmapData), MEMF_CHIP | MEMF_CLEAR);
	
	// Load background into bitmap
	printf("Load bitmap: %s\n",filename);
	IFFL_HANDLE iff = IFFL_OpenIFF(filename, IFFL_MODE_READ);
	if(iff == NULL)
	{
        printf("IFF open failed :(\n");
        return FALSE;
	}		

	struct IFFL_BMHD *bmhd = IFFL_GetBMHD(iff);
	if(bmhd==NULL)
	{
		printf("IFF is not bitmap :(\n");
        return FALSE;
	}
	
	bitmapData->cm = GetColorMap(32);
	if (bitmapData->cm == NULL)
	{
		printf("Could not get ColorMap\n");
        return FALSE;
	}
	
	// Load the background colour map
	bitmapData->colorTableSize = IFFL_GetColorTab(iff, bitmapData->colortable);	
	printf("CTSize: %d\n",bitmapData->colorTableSize);
	//for(int i=0;i<16;i++)
	//{
	//	printf("%d: %x\n",i,bitmapData->colortable[i]);
	//}
	
	// Allocate the bitmap and decode it
	bitmapData->planes = bmhd->nPlanes;
	printf("Btmap: %dx%d @ %d planes\n",bmhd->w, bmhd->h, bmhd->nPlanes);
	bitmapData->bitmap = AllocBitMap(bmhd->w, bmhd->h, bmhd->nPlanes, BMF_CLEAR | BMF_DISPLAYABLE, NULL);
	if(!IFFL_DecodePic(iff, bitmapData->bitmap))
	{
		printf("Could not decode map bitmap\n");
        return FALSE;
	}
	
	bitmapData->rasInfo.BitMap = bitmapData->bitmap;
	bitmapData->rasInfo.RxOffset = 0;
	bitmapData->rasInfo.RyOffset = 0;
	
	return bitmapData;
}

void UnloadBitmapData(struct BitmapData* bitmapData)
{
	if(bitmapData == NULL) return;
	
	if(bitmapData->cm) 
	{
		FreeColorMap(bitmapData->cm);
		bitmapData->cm = NULL;
	}
	
	if(bitmapData->bitmap) 
	{
		FreeBitMap(bitmapData->bitmap);
		bitmapData->bitmap = NULL;
	}
}	