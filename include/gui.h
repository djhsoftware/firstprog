#ifndef __GUI_H
#define __GUI_H

#include "graphics.h"
#include "lib/iff.h"
#include <proto/iff.h>
#include <proto/exec.h>
#include <stdio.h>
#include <string.h>
#include "util.h"

struct GuiData {
	struct BitmapData *bd1;
	int height;
};

// Read Gui 
struct GuiData *LoadGui();

// Unload Gui
void UnloadGui(struct GuiData *gui);

#endif