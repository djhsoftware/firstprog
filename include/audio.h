#ifndef __AUDIO_H
#define __AUDIO_H

#include <proto/exec.h>
#include <proto/ptreplay.h>
#include <stdio.h>
#include "lib/sound_routines.h"
#include "lib/ptreplay.h"

struct AudioData
{
	// Single audio module
	struct Module *module;
	unsigned char *modData;
	long modDataSize;
	
	// TODO: Replace with array of samples
	struct SoundInfo *background;
	struct SoundInfo *snd_ah;
};

// Initialise the Audio
int InitAudio(struct AudioData *audio);

// Load Audio
// TODO: Load from map data
int LoadAudio(struct AudioData *audio);

// Play Audio
int PlayAudio(struct AudioData *audio, int id, UWORD volume, UBYTE channel, WORD delta_rate, UWORD repeat);

// Unload Audio
void UnloadAudio(struct AudioData *audio);

#endif