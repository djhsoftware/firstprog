#ifndef __UTIL_H
#define __UTIL_H

#include "graphics.h"
#include "lib/iff.h"
#include <proto/iff.h>
#include <proto/exec.h>
#include <stdio.h>

struct BitmapData
{
	int width;
	int height;
	
	// Color data
	struct ColorMap* cm;
	UWORD colortable[16];
	LONG colorTableSize;
	
	// The bitmap data
	UBYTE planes;
	struct BitMap *bitmap;
	
	struct RasInfo rasInfo;
};

unsigned char *LoadFileToFast(char *filename);

struct BitmapData *LoadBitmapData(char *filename);

struct BitmapData *CreateBitmapData(int width, int height, UBYTE planes);

void UnloadBitmapData(struct BitmapData* bitmapData);

#endif