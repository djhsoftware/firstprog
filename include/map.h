#ifndef __MAP_H
#define __MAP_H

#include <stdio.h>
#include <proto/exec.h>
#include "graphics.h"
#include "tileset.h"
#include "util.h"

/// TODO: use char instead of int for map size
struct MapData {
	int width;
	int height;	
	char name[20];
	char tileset[30];
	char background[30];	
	unsigned char *data;
	
	// The Tileset, if we need it
	struct TileSet *tiledata;
	
	// Bitmap data
	struct BitmapData *bd1;
	struct BitmapData *bd2;
};

// Read Map 
struct MapData *LoadMap(char *filename);

// Render map
BOOL RenderMap(struct MapData *map);

// Unload map
void UnloadMap(struct MapData *map);

#endif