#ifndef __MAIN_H
#define __MAIN_H

#include <stdio.h>
#include <stdlib.h>
#include <hardware/custom.h>
#include <hardware/cia.h>
#include <hardware/dmabits.h>

#include <proto/exec.h>
#include <proto/intuition.h>

#ifdef __GFX_BASE
#include <graphics/gfxbase.h>
struct GfxBase *GfxBase;
#endif

#include "screen.h"
#include "graphics.h"

#define TASK_PRIORITY 	20

struct IFFBase *IFFBase;
struct Library *PTReplayBase;

extern struct Custom custom;
extern struct CIA ciaa, ciab;

struct View view;
struct ViewPort viewPort;
struct ViewPort viewPortTop;

int frame;

void InitLibs(void);

int InitPlayField(void);

void InitSound(void);

void Cleanup(STRPTR message);

void GameLogic(void);

void DrawPlayField(void);

void LoadBitmap(void);

int MouseClick(void);

unsigned char *LoadFileToFast(char *filename);

#endif