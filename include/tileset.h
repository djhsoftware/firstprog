#ifndef __TILESET_H
#define __TILESET_H

#ifdef __GFX_BASE
#include <graphics/gfxbase.h>
#endif

#include "graphics.h"
#include "lib/iff.h"
#include <proto/iff.h>
#include <proto/exec.h>
#include <stdio.h>
#include <string.h>
#include "util.h"

struct TileSet
{
	// The name of the tileset
	char name[20];
	
	struct BitmapData *bd;
};

// Load tileset from file
struct TileSet *LoadTileSet(char *filename);

// Unload tileset
void UnloadTileSet(struct TileSet *tileset);

#endif