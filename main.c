#include "main.h"
#include "map.h"
#include "audio.h"
#include "gui.h"

// TEMPORARY STUFF
__chip UWORD sprite_data[] =
{
    0, 0,           /* position control           */
    0xff2f, 0x0000, /* image data line 1, color 1 */
    0xf2ff, 0x0000, /* image data line 2, color 1 */
    0x0000, 0xffff, /* image data line 3, color 2 */
    0x0000, 0xffff, /* image data line 4, color 2 */
    0x0000, 0x0000, /* image data line 5, transparent */
    0x0000, 0xffff, /* image data line 6, color 2 */
    0x0000, 0xffff, /* image data line 7, color 2 */
    0xf1ff, 0xf1ff, /* image data line 8, color 3 */
    0xffff, 0xffff, /* image data line 9, color 3 */
    0, 0            /* reserved, must init to 0 0 */
};

struct SimpleSprite sprite = {0};

WORD sprite_num;
// END TEMPORARY

// Gui data
struct GuiData *gui;

// Currently loaded map
struct MapData *map;

// Audio for level
struct AudioData audio;

// TODO: Replace with tables
int rasScrollCurrent = 0;
int rasScrollMax = 320;
int rasScrollSpeed = 1;

int main(int argc, char **argv)
{	
	// Initialise
	InitLibs();
	InitAudio(&audio);
	
	// Init initial map | audio	
	map = LoadMap("res/map/map.001");
	if(map == NULL)
	{
        Cleanup("Map Load Failed");
		return -1;		
	}
	
	// Read GUI
	gui = LoadGui();
	if(gui == NULL)
	{
        Cleanup("GUI Load Failed");
		return -2;		
	}
	
	// Read audio
	if(LoadAudio(&audio)!=0)
	{
        Cleanup("Audio Load Failed");
		return -3;
	}
		
    if (InitPlayField() == -1)
    {
        Cleanup("InitPlayField: Failed");
        return -4;
    }
	
    while (!MouseClick())
    {
		//frame++;
		//if (++frame == 50) frame = 0;
		GameLogic();
		WaitTOF();
		//DrawPlayField();
    }

    Cleanup("BYE BYE");
}

void InitLibs(void)
{
	
#ifdef __GFX_BASE
	/* Open the Graphics library */
	GfxBase = (struct GfxBase *)OpenLibrary( "graphics.library", 0 );
	if( !GfxBase )
		Cleanup( "Could NOT open the Graphics library!" );
#endif

	// Open iff.library
	IFFBase = (struct IFFBase *)OpenLibrary("iff.library", 0);
	if( !IFFBase )
		Cleanup( "Could NOT open the iff library!" );
}

int InitPlayField(void)
{
	printf("InitPlayField\n");
	
	//custom.dmacon = BITSET|DMAF_SPRITE;
	
	// Prepare View
    InitView( &view );
	view.ViewPort = &viewPortTop;
		
	// Prepare ViewPort s
	InitVPort( &viewPortTop );
	
	viewPortTop.DWidth = DISPL_WIDTH;
	viewPortTop.DHeight = 20;
	viewPortTop.DxOffset = 0;
	viewPortTop.DyOffset = 0;
	viewPortTop.RasInfo = &gui->bd1->rasInfo;
	viewPortTop.Modes = 0;
	viewPortTop.ColorMap = gui->bd1->cm;
	viewPortTop.Next = &viewPort;
	
	InitVPort( &viewPort );
	viewPort.DWidth = DISPL_WIDTH;
	viewPort.DHeight = DISPL_HEIGHT-21;
	viewPort.DxOffset = 0;
	viewPort.DyOffset = 21;
	viewPort.RasInfo = &map->bd1->rasInfo;
	viewPort.Modes =  DUALPF | PFBA | SPRITES;
	viewPort.ColorMap = map->bd1->cm;
	viewPort.Next = NULL;
		
	// Load color map
	LoadRGB4(&viewPortTop, gui->bd1->colortable, gui->bd1->colorTableSize);
	LoadRGB4(&viewPort, map->bd1->colortable, map->bd1->colorTableSize);
	
	// Create the view ports
	MakeVPort(&view, &viewPortTop); 
	MakeVPort(&view, &viewPort);
	
	// Assign to view
	
	/*
	sprite_num = GetSprite(&sprite, -1);
	printf("GetSprite returned %d\n",sprite_num);
	
	if(sprite_num > 0)
	{
		sprite.x = 30;       // initialize position and size info    
		sprite.y = 30;       // to match that shown in sprite_data   
		sprite.height = 9;  // so system knows layout of data later 

		// install sprite data and move sprite to start position. 
		ChangeSprite(NULL, &sprite, (APTR)sprite_data);
		//MoveSprite(NULL, &sprite, 30, 30);
	}*/
	
	// Merge to Copper
	MrgCop(&view);

	// Show the View
	LoadView( &view );	
  
	WaitTOF();
	
    return 0;
}

void GameLogic(void)
{
	// Background scroll is keyed off front
	// TODO: Need to get scroll values from rasinfo
	
	if(map->bd2->rasInfo.RxOffset < 0)
	{
		rasScrollSpeed *= -1;
	}
	
	if(map->bd2->rasInfo.RxOffset > rasScrollMax)
	{
		rasScrollSpeed *= -1;
	}
	
	rasScrollCurrent+=rasScrollSpeed;
	map->bd1->rasInfo.RxOffset=rasScrollCurrent/2;
	map->bd2->rasInfo.RxOffset=rasScrollCurrent;
	
	ScrollVPort(&viewPort);
}

void DrawPlayField(void)
{
}

void Cleanup(STRPTR message)
{
	printf("Cleanup\n");
	
#ifdef __GFX_BASE
    LoadView(((struct GfxBase *)GfxBase)->ActiView);
    custom.cop1lc = (ULONG)((struct GfxBase *)GfxBase)->copinit;
	CloseLibrary((struct Library *)GfxBase);
#endif

	WaitTOF();
    WaitTOF();
    RethinkDisplay();
	
	CloseLibrary((struct Library *)IFFBase);
	
	custom.dmacon = BITSET|DMAF_SPRITE;
	FreeSprite(sprite_num);
	    
	// Free memory
	UnloadMap(map);
	UnloadAudio(&audio);
	UnloadGui(gui);	
	
	printf( "%s\n", message );
	exit(0);
}

int MouseClick(void)
{
    return !(ciaa.ciapra & CIAF_GAMEPORT0);
}