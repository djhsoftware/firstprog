#include "gui.h"

/* =========================================
   Load Gui
   =========================================*/
struct GuiData *LoadGui()
{ 
	printf("Loading gui\n");
	struct GuiData *gui = AllocMem(sizeof(struct GuiData), MEMF_CHIP | MEMF_CLEAR);
	gui->bd1 = LoadBitmapData("res/gfx/gui.iff");	
	gui->height = 20;
	
	return gui;
}

/* =========================================
   Unloads the Gui struct
   =========================================*/
void UnloadGui(struct GuiData *gui)
{
	if(gui != NULL)
	{
		if(gui->bd1)
		{
			UnloadBitmapData(gui->bd1);
		}
		
		FreeMem(gui, sizeof(struct GuiData));
	}
}