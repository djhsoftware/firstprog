#include "map.h"

/* =========================================
   Reads map data into 1 byte per tile
   (256 tile types - overkill for now)
   =========================================*/
struct MapData *LoadMap(char *filename)
{ 
	printf("Loading map: %s\n",filename);
	struct MapData *map = AllocMem(sizeof(struct MapData), MEMF_CHIP | MEMF_CLEAR);
	if(map==NULL)
	{
		return NULL;
	}
	
	char line[20];
	char linePos = 0;
	line[0] = '\0';
	
	char *mapPtr;
	char ch;
	FILE *fp;
	fp = fopen(filename, "r");

	char readMode = '\0';
	int readLine = 0;
	
	// Parse the map data
	while((ch = fgetc(fp)) != EOF)
	{
		// HEADER INDICATOR
		if(ch=='\r')
		{
		}
		else if(ch=='#')
		{
			readMode = fgetc(fp);
			readLine = 0;
		}
		else if(ch=='\n')
		{
			if(readMode=='C')
			{
				// Comment section is ignored - output to screen for now
				printf("%s\n", line);
			}
			else if(readMode=='H')
			{
				// Null terminate the header
				line[linePos]	= '\0';
				switch(readLine)
				{
					case 1:
						// First line is map name
						strcpy(map->name, line);
						printf("Name: %s\n", line);
						break;
					case 2:
						// Second line is size and tileset file
						
						// Scan header for width / height / tile iff name
						sscanf(line,"%d %d %s %s", &map->width, &map->height, map->tileset, map->background);
						
						// Allocate space for map data (TODO: DEALLOCATE THIS LATER!)
						printf("Allocating space for %d x %d map\n", map->width, map->height);
						printf("Scaled up this will created %d x %d playfield\n", map->width*16, map->height*16);
						map->data = (unsigned char *)AllocMem(map->width * map->height, MEMF_FAST | MEMF_CLEAR);
						
						// Set map data pointer to start of memory
						mapPtr = map->data;
						break;
				}
				
			}
			else if(readMode=='X')
			{
				printf("END\n");
			}
			else if(readMode=='A' && readLine > 0)
			{
				int id;
				char name[20];
				sscanf(line,"%d %s", &id, name);
				printf("Audio Id %d: %s\n", id, name);
			}	
			
			linePos = 0;
			line[0] = '\0';
			readLine++;
		}
		else if(readMode=='M')
		{
			*mapPtr = ch-48;
			mapPtr++;
		}
		else
		{
			line[linePos++]=ch;
			line[linePos]='\0';
		}
	} 
	
	fclose(fp);
	
	// Back to start of map data so we can review it (debug only)
	mapPtr = map->data;
	for(int y=0;y < map->height;y++)
	{
		for(int x=0;x < map->width;x++)
		{
			printf("%d", *mapPtr++);
		}
		
		printf("\n");
	}
	
	// Render the map
	if(RenderMap(map))
	{
		return map;
	}
	else
	{
		UnloadMap(map);
		return NULL;
	}	
}

/* =========================================
   Renders the map using the supplied tileset
   =========================================*/
BOOL RenderMap(struct MapData *map)
{	
	printf("RenderMap\n");

	// map width in pixels
	int bWidth = map->width * 16;
	int bHeight = map->height * 16;	
	
	// Load map background
	printf("LoadBackground\n");
	map->bd1 = LoadBitmapData(map->background);	
	
	// Load foreground tileset
	printf("LoadTileSet\n");
	map->tiledata = LoadTileSet(map->tileset);
	if(map->tiledata == NULL)
	{
        printf("TileSet load failed");
		return FALSE;
	}
	
	// Create foreground empty bitmap
	printf("AllocBitMap %dx%d @ %d planes\n", bWidth, bHeight, map->bd1->planes);
	map->bd2 = CreateBitmapData(bWidth, bHeight, map->bd1->planes);
	
	// Link RasInfo for dual playfield
	map->bd1->rasInfo.Next = &map->bd2->rasInfo;
	
	// Blit the tiles to the new foreground bitmap
	printf("BlitTiles\n");
	char *mapPtr = map->data;
	for(int my = 0;my < map->height; my++)
	{
		for(int mx = 0;mx < map->width; mx++)
		{
			int cell = *mapPtr++;
			if(cell > 0)
			{
				BltBitMap(map->tiledata->bd->bitmap, cell * 16, 0, map->bd2->bitmap, mx*16, my*16, 16, 16, 0xc0, 0xff, 0);
			}
		}
	}
	
	//printf("BEFORE\n");
	//for(int i=0;i<16;i++)
	//{
	//	printf("%d: %x ; %x\n",i,map->bd1->colortable[i], map->tiledata->bd->colortable[i]);
	//}
	
	// Copy first half of tile colour map to second half of bd1 colour map
	printf("Palette Swap\n");
	memcpy(&map->bd1->colortable[8], &map->tiledata->bd->colortable[0], 16);
	
	//printf("AFTER\n");
	//for(int i=0;i<16;i++)
	//{
	//	printf("%d: %x ; %x\n",i,map->bd1->colortable[i], map->tiledata->bd->colortable[i]);
	//}
	
	// Finished with tileset now
	// But we may need to keep it if we want to dynamically change the map later!
	UnloadTileSet(map->tiledata);
	map->tiledata = NULL;
	
	return TRUE;
}

/* =========================================
   Unloads the map struct
   =========================================*/
void UnloadMap(struct MapData *map)
{
	if(map != NULL)
	{
		if (map->data) {
			FreeMem(map->data, map->width * map->height);	
		}	
		
		UnloadTileSet(map->tiledata);
		UnloadBitmapData(map->bd1);
		UnloadBitmapData(map->bd2);
		FreeMem(map, sizeof(struct MapData));
	}
}