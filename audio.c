#include "audio.h"
#include "util.h"

// Initialise the Audio
int InitAudio(struct AudioData *audio)
{	
	audio->modData = NULL;
	audio->modDataSize = 0;
	audio->module = NULL;
	audio->background = NULL;
	audio->snd_ah = NULL;
	
	// Open ptreplay.library
    PTReplayBase = OpenLibrary((UBYTE *)"ptreplay.library", 0);
	if( !PTReplayBase )
	{
		printf( "Could NOT open the ptreplay library!" );
		return -1;
	}
	
	return 0;
}

// Load Audio
int LoadAudio(struct AudioData *audio)
{
	printf("Init sound\n");
	
	audio->snd_ah = PrepareSound( "res/sfx/snd.ah" );
	if( !audio->snd_ah )
	{
		printf( "Could not load ah.iff" );
		return -1;
	}
	
	audio->background = PrepareSound( "res/sfx/music_loop.8svx" );
	if( !audio->background )
	{
		printf( "Could not load loop.iff" );
		return -2;
	}
	
    //audio->modData = LoadFileToFast("res/mod/mod.boo");
    //audio->module = PTSetupMod((APTR)audio->modData);
    //PTPlay(audio->module);
	
	//PlaySound( background, MAXVOLUME, LEFT0, NORMALRATE, NONSTOP );
	//PlaySound( background, MAXVOLUME, RIGHT0, NORMALRATE, NONSTOP );
	
	return 0;
}

int PlayAudio(struct AudioData *audio, int id, UWORD volume, UBYTE channel, WORD delta_rate, UWORD repeat)
{
	switch(id)
	{
		case 0:
			PlaySound( audio->background, volume, channel, delta_rate, repeat );	
			break;
		case 1:
			PlaySound( audio->snd_ah, volume, channel, delta_rate, repeat );	
			break;
	}	

	return 0;	
}

// Unload Audio
void UnloadAudio(struct AudioData *audio)
{
	if (audio->background)
		RemoveSound( audio->background );
	
	if (audio->snd_ah)
		RemoveSound( audio->snd_ah );
	
	StopSound( LEFT0 );
	StopSound( RIGHT0 );
	StopSound( LEFT1 );
	StopSound( RIGHT1 );
		
    if (audio->modData)
    {
		FreeMem(audio->modData, audio->modDataSize);
	}
	
	if(audio->module)
	{
		PTStop(audio->module);
		PTFreeMod(audio->module);
    }
	
	if(PTReplayBase)
		CloseLibrary((struct Library *)PTReplayBase);
}