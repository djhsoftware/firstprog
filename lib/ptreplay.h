#ifndef _PROTO_PTREPLAY_H
#define _PROTO_PTREPLAY_H

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif
#if !defined(CLIB_PTREPLAY_PROTOS_H) && !defined(__GNUC__)
#include <clib/ptreplay_protos.h>
#endif

#ifndef __NOLIBBASE__
extern struct Library *PTReplayBase;
#endif

#ifdef __GNUC__
#include <inline/ptreplay.h>
#elif defined(__VBCC__)
#if defined(__MORPHOS__) || !defined(__PPC__)
#include <inline/ptreplay_protos.h>
#endif
#else
#include <pragma/ptreplay_lib.h>
#endif

#endif	/*  _PROTO_PTREPLAY_H  */
