#include "tileset.h"

/* =========================================
   Read tileset from file
   =========================================*/
struct TileSet *LoadTileSet(char *filename)
{
	struct TileSet *tileset = AllocMem(sizeof(struct TileSet), MEMF_CHIP | MEMF_CLEAR);
	
	// Load the bitmap
	tileset->bd = LoadBitmapData(filename);
	
	// Set the tileset name
	strcpy(tileset->name, filename);
	
	return tileset;
}

/* =========================================
   Unload the tile set and free memory
   =========================================*/
void UnloadTileSet(struct TileSet *tileset)
{
	if(tileset)
	{
		UnloadBitmapData(tileset->bd);
		FreeMem(tileset,sizeof(struct TileSet));
	}
}