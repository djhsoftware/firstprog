CC=vc
LDFLAGS=-lamiga -lauto -lmieee
INC=-Iinclude
EXE=bin/game
SRC=main.c \
    gui.c \
	map.c \
	tileset.c \
	audio.c \
	util.c \
	lib\sound_routines.c
	
UAE=D:\\media\\Software\\Emulation\\Amiga\\Harddrives\Applications\\dev\\firstprog\\

all: 
	@rmdir /s/q bin
	@mkdir bin\res
	$(CC) $(SRC) $(INC) $(LDFLAGS) -o $(EXE)	
	@rmdir /s/q $(UAE)
	@mkdir $(UAE)
	@xcopy /S/Q/Y res\\* bin\\res
	@xcopy /S/Q/Y bin\\* $(UAE)
